import 'package:meta/meta.dart';

class Item {
  final String itemName;
  final double price;
  final double reviews;
  final String itemImg;

  const Item({
    @required this.itemName,
    @required this.itemImg,
    this.price: 0.0,
    this.reviews,
  });
}
