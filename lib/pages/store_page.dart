import 'package:flutter/material.dart';
import 'package:flutter_market/models/item.dart';

class StorePage extends StatefulWidget {
  @override
  _StorePageState createState() => _StorePageState();
}

class _StorePageState extends State<StorePage> {
  List<String> tabs = [
    'Featured',
    'App clones',
    'Widgets',
    'Fullstack apps',
    'Concept apps',
    'Animations',
    'Other'
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Flutter Market',
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 26.0),
          ),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search, color: Colors.black),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.account_circle, color: Colors.black),
            ),
          ],
          bottom: TabBar(
            isScrollable: true,
            tabs: tabs.map<Widget>((tabName) => Tab(text: tabName)).toList(),
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                FeaturedItem(
                    item: Item(
                        itemName: 'Whatsapp Clone',
                        itemImg: 'res/app_1.jpg',
                        reviews: 3.6)),
                FeaturedItem(
                    item: Item(
                        itemName: 'Facebook Clone',
                        itemImg: 'res/app_2.png',
                        price: 9.99)),
                FeaturedItem(
                    item: Item(
                        itemName: 'Uber Clone',
                        itemImg: 'res/app_1.jpg',
                        reviews: 2.9)),
                FeaturedItem(
                    item: Item(
                        itemName: 'Travel concept app',
                        itemImg: 'res/app_1.jpg',
                        reviews: 2.6)),
              ],
            ),
            Center(),
            Center(),
            Center(),
            Center(),
            Center(),
            Center(),
          ],
        ),
      ),
    );
  }
}

class FeaturedItem extends StatelessWidget {
  final Item item;
  const FeaturedItem({@required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(12.0),
      child: Material(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
        elevation: 10.0,
        shadowColor: Colors.black38,
        child: Column(
          children: <Widget>[
            SizedBox.fromSize(
              size: Size.fromHeight(96.0),
              child: Image.asset(item.itemImg, fit: BoxFit.cover),
            ),
            Container(
              margin: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            item.itemName,
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20.0),
                          ),
                          Text('Made by Ivascu Adrian'),
                        ],
                      ),
                      Text(
                        item.price == 0.0
                            ? 'FREE'
                            : r'$' + item.price.toStringAsFixed(2),
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Theme.of(context).accentColor),
                      )
                    ],
                  ),
                  SizedBox(height: 4.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      OutlineButton(
                        onPressed: () {},
                        child: Text('More infos'),
                        highlightedBorderColor: Theme.of(context).accentColor,
                        highlightColor: Colors.transparent,
                      ),
                      _getStarsRow()
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getStarsRow() {
    if (item.reviews != null) {
      bool halfStar = false;

      return Row(
          children: Iterable.generate(5, (i) {
        if (i + 1 < item.reviews)
          return Icon(Icons.star);
        else if (!halfStar && item.reviews - item.reviews.truncate() >= 0.8) {
          halfStar = true;
          return Icon(Icons.star);
        } else if (!halfStar && item.reviews - item.reviews.truncate() >= 0.3) {
          halfStar = true;
          return Icon(Icons.star_half);
        } else
          return Icon(Icons.star_border);
      }).toList());
    } else
      return Text('Not enough reviews');
  }
}
