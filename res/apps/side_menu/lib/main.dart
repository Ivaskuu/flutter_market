import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Side menu',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  bool _showMenu = false;
  AnimationController _controller;

  Tween<double> _scaleTween;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _controller.addListener(() => setState(() {}));

    _scaleTween = Tween<double>(begin: 1.0, end: 0.6);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onHorizontalDragUpdate: (details) {
          _controller.value += details.primaryDelta / 200.0;
          print(_controller.value);
          // _controller.value.clamp(0.0, 1.0);
        },
        onHorizontalDragEnd: (_) {
          if ((!_showMenu && _controller.value > 0.1) ||
              (_showMenu && _controller.value > 0.9)) {
            _controller.forward();
          } else
            _controller.reverse();

          _showMenu = !_showMenu;
        },
        child: Stack(
          children: <Widget>[
            _buildMenu(),
            Transform(
              alignment: Alignment.centerRight,
              transform: Matrix4.identity()
                ..scale(_scaleTween
                    .animate(
                      CurvedAnimation(parent: _controller, curve: Curves.ease),
                    )
                    .value)
                ..translate(
                  CurvedAnimation(parent: _controller, curve: Curves.ease)
                          .value *
                      MediaQuery.of(context).size.width /
                      2.0,
                ),
              child: Material(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(_controller.value * 16.0),
                child: Scaffold(
                  appBar: AppBar(
                    leading: IconButton(
                      onPressed: () {
                        setState(() {
                          _showMenu = !_showMenu;
                          if (_showMenu)
                            _controller.forward();
                          else
                            _controller.reverse();
                        });
                      },
                      icon: Icon(Icons.menu),
                    ),
                  ),
                  body: Center(
                    child: Text('Content'),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMenu() {
    return SizedBox.expand(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF4B5388),
              Color(0xFF4F76B8),
            ],
            end: Alignment.topLeft,
            begin: Alignment.bottomRight,
          ),
        ),
        padding: EdgeInsets.only(
          top: 32.0,
          bottom: 16.0,
          left: 8.0,
          right: MediaQuery.of(context).size.width * 0.3 + 12.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              radius: 48.0,
              backgroundColor: Colors.white10,
              child: Icon(
                Icons.account_circle,
                size: 64.0,
                color: Colors.white,
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'GEORGE SAMUEL',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '@sgeorge699',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            SizedBox(height: 32.0),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    _buildMenuTile(Icons.work),
                    _buildMenuTile(Icons.email),
                  ],
                ),
                Row(
                  children: <Widget>[
                    _buildMenuTile(Icons.bubble_chart),
                    _buildMenuTile(Icons.multiline_chart),
                  ],
                ),
                Row(
                  children: <Widget>[
                    _buildMenuTile(Icons.rate_review),
                    _buildMenuTile(Icons.notification_important, red: true),
                  ],
                ),
                Row(
                  children: <Widget>[
                    _buildMenuTile(Icons.settings),
                    _buildMenuTile(Icons.exit_to_app),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMenuTile(IconData icon, {bool red: false}) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(8.0),
        child: Material(
          color: red ? Colors.pinkAccent : Colors.white10,
          borderRadius: BorderRadius.circular(8.0),
          clipBehavior: Clip.antiAlias,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 24.0),
            child: Icon(icon, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
