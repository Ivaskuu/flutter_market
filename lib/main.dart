import 'package:flutter/material.dart';
import 'pages/store_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Market',
      theme: ThemeData(
        fontFamily: 'Google Sans',
        accentColor: Colors.greenAccent[400],
        primaryColor: Colors.white,
        backgroundColor: Colors.white,
      ),
      home: StorePage(),
    );
  }
}